import { StudentsViewComponent } from './view/students.view.component';
import { Routes, RouterModule } from '@angular/router';
import { StudentsAddComponent } from './add/students.add.component';
import { StudentsComponent } from './list/students.component';
import { NgModule } from '@angular/core';

const StudentRoutes: Routes = [
    {path: 'view', component: StudentsViewComponent},
    {path: 'add', component: StudentsAddComponent},
    {path: 'list', component: StudentsComponent},
    {path: 'detail/:id', component: StudentsViewComponent}
];
@NgModule({
    imports: [
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class StudentRoutingModule {

}